# Radar Target Generation and Detection

## FMCW Waveform Design
Bandwidth = 150 MHz  
Chirp time = 7.333 us  
Slope of the chirp = 2.0455e+13  

## Simulation Loop
```matlab
for i=1:length(t)         
    %For each time stamp update the Range of the Target for constant velocity. 
    r_t(i) = (target_velocity * t(i)) + target_position;
    td(i) = (2 * r_t(i)) / speedOfLight;

    %For each time sample we need update the transmitted and
    %received signal. 
    Tx(i) = cos(2 * pi * (fc * t(i) + (slope * t(i) ^ 2) / 2.0));
    Rx(i) = cos(2 * pi * (fc * (t(i) - td(i)) + (slope * (t(i) - td(i)) ^ 2) / 2.0));
    
    %Now by mixing the Transmit and Receive generate the beat signal
    %This is done by element wise matrix multiplication of Transmit and
    %Receiver Signal
    
    Mix(i) = Tx(i).*Rx(i);
end
```

## Range FFT (1st FFT)
```matlab
%% RANGE MEASUREMENT

 % *%TODO* :
%reshape the vector into Nr*Nd array. Nr and Nd here would also define the size of
%Range and Doppler FFT respectively.
Mix = reshape(Mix, [Nr, Nd]);

 % *%TODO* :
%run the FFT on the beat signal along the range bins dimension (Nr) and
%normalize.
signal_fft = fft(Mix,Nr);
signal_fft = signal_fft / Nr;

 % *%TODO* :
% Take the absolute value of FFT output
signal_fft = abs(signal_fft);

 % *%TODO* :
% Output of FFT is double sided signal, but we are interested in only one side of the spectrum.
% Hence we throw out half of the samples.
signal_fft = signal_fft(1:Nr / 2);

%plotting the range
figure ('Name','Range from First FFT')

plot(signal_fft);
 
axis ([0 200 0 0.5]);
```

Result:
![](Images/1D_FFT.png)

## 2D CFAR

![](Images/2D_FFT.png)

### Implementation steps for the 2D CFAR process
1. Zero matrix 'RDMFiltered' with the same size as RDM is created.
2. Cell under test is moved across the matrix while keeping margin for training na guard cells.
3. Submatrix 'window' is created from 'RDM' matrix. Submatrix contains CUT plus guard cells and training cells.
4. Values of guarding cells and CUT are set to zero in 'window' submatrix so they will not affect total sum.
5. Average value of training cells in dB is computed.
6. If value of the CUT is larger than average plus offset, corresponding cell in the 'RDMFiltered' matris is set to 1.

```matlab
RDMFiltered = zeros(size(RDM));

train_count = (2 * Tc + 2 * Gc + 1) * (2 * Tr + 2 * Gr +1) - (2 * Gc + 1) * (2 * Gr + 1);
for ri = (Tr + Gr + 1):(RDM_rows - Tr - Gr - 1)
    for ci = (Tc + Gc + 1):(RDM_cols - Tc - Gc - 1)
        window = RDM(ri - Tr - Gr:ri + Tr + Gr, ci - Tc - Gc:ci + Tc + Gc);
        window(Tr + 1:Tr + 2 * Gr + 1, Tc + 1:Tc + 2 * Gc + 1) = 0;
        powsum = sum(db2pow(window), 'all');
        avgdb = pow2db(powsum / train_count);
        if (RDM(ri, ci) > (avgdb + offset))
            RDMFiltered(ri, ci) = 1;
        end
    end
end
```

### Selection of Training, Guard cells and offset

Offset, amount of training cells and guard cells was determined experimentally. These values provide one peak located around position that represents target range and velocity.

```matlab
Tc = 10;  % Training cells columns
Tr = 10;  % Training cells row 
Gc = 2;   % Guard cells columns
Gr = 2;   % Guard cells rows
offset = 6;  % dB
```
### Steps taken to suppress the non-thresholded cells at the edges

Non-thresholded cells are set to zero by initializing the 'RDMFiltered' matrix before iteration over 'RDM' matrix starts.

![](Images/2D_filtered.png)